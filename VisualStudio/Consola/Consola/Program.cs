﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            Suma operaciones1 = new Suma();
            Resta operaciones2 = new Resta();
            Multi operaciones3 = new Multi();
            Div operaciones4 = new Div();
            int resultado1 = int.Parse ("operaciones1.sum()");
            int resultado2 = int.Parse ("operaciones2.res()");
            int resultado3 = int.Parse ("operaciones3.mult()");
            int resultado4 = int.Parse ("operaciones4.div()");

            Console.WriteLine("El resultado de la suma es: {resultado1}");
            Console.WriteLine("El resultado de la resta es: {resultado2}");
            Console.WriteLine("El resultado de la multiplicación es: {resultado3}");
            Console.WriteLine("El resultado de la división es: {resultado4}");
            System.Console.ReadKey();

        }
    }
}
