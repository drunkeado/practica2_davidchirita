package main;

import libreria.*;
import java.util.Scanner;

public class Main {
		
	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		int opcion;
		do{
			System.out.println("\t      MEN�"
					+ "\n-------- Elija una opci�n --------"
					+ "\n1. Averiguar la media."
					+ "\n2. Factorial de un n�mero."
					+ "\n3. N�mero m�ximo."
					+ "\n4. N�mero primo."
					+ "\n5. Salir.");
			opcion=in.nextInt();
			}while(opcion<1 || opcion>5);
			
			if (opcion==1){
				Media.media();	
			}	
			else if(opcion==2){
				Factorial.factorial();
			}		
			else if(opcion==3){
				Max.max();
			}
			else if(opcion==4){
				Primo.primo();
			}
			else{
				System.out.println("FIN DEL PROGRAMA");
			}
		in.close();
	}
}
